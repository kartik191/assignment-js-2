function stringToNumber(string){
    if(typeof(string) !== 'string'){
        return 0;
    }
    let neg = false;
    if(string[0] ===  '-'){
        neg = true;
        string = string.substring(1);
    }
    if(string[0] === '$'){
        string = string.substring(1);
    }
    string = string.replace(',', '');
    if(neg){
        string = '-' + string;
    }
    if(isNaN(string)){
        return 0;
    }
    else{
        return Number(string);
    }
}

module.exports = { stringToNumber };
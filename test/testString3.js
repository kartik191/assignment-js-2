const string3 = require('../string3');


let string = '2/09/2021';
let month;
try{
    month = string3.getMonth(string);
    console.log(month); 
}
catch(e){
    console.error(e);
}


try{
    string = '12/09/2021';
    month = string3.getMonth(string);
    console.log(month);
}
catch(e){
    console.error(e);
}

try{
    string = '01/09/2021';
    month = string3.getMonth(string);
    console.log(month);
}
catch(e){
    console.error(e);
}


try{
    string = '0/09/2021';
    month = string3.getMonth(string);
    console.log(month);
}
catch(e){
    console.error(e);
}

try{
    string = '13/09/2021';
    month = string3.getMonth(string);
    console.log(month);
}
catch(e){
    console.error(e);
}


try{
    string = '1/09/202';
    month = string3.getMonth(string);
    console.log(month);
}
catch(e){
    console.error(e);
}


try{
    string = '1/09/2022/';
    month = string3.getMonth(string);
    console.log(month);
}
catch(e){
    console.error(e);
}


try{
    string = '1//2022/';
    month = string3.getMonth(string);
    console.log(month);
}
catch(e){
    console.error(e);
}


try{
    string = '1/bv/2022/';
    month = string3.getMonth(string);
    console.log(month);
}
catch(e){
    console.error(e);
}


try{
    string = 234;
    month = string3.getMonth(string);
    console.log(month);
}
catch(e){
    console.error(e);
}

try{
    month = string3.getMonth();
    console.log(month);
}
catch(e){
    console.error(e);
}